@echo off
:: ========================================================================
:: $File: setup.bat $
:: $Date: 2019-12-10 22:58:49 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2019 by Shen, Jen-Chieh $
:: ========================================================================

cd ..

npm install sass -g
