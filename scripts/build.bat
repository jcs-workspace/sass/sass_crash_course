@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2019-12-10 23:00:34 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2019 by Shen, Jen-Chieh $
:: ========================================================================

cd ..

sass "./styles/style.scss" "./styles/style.css"
